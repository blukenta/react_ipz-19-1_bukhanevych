import React from 'react';

const Price = ({...prices}) => {
    if (prices.oldPrice) {
        return (
            <div>
                <div className="old-price">{prices.oldPrice}<span className="uah-small"> ₴</span></div>
                <div className="price">{prices.price}<span className="uah"> ₴</span></div>
            </div>
        );
    } else return <div className="price no-old-price">{prices.price}<span className="uah"> ₴</span></div>
};

export default Price;