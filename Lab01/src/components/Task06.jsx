import React, {useState} from 'react';

const Task06 = (props) => {
    const colors = ['blue', 'yellow', 'green', 'red'];
    const [value, setValue] = useState('blue');

    const options = colors.map((col, index) => {
        return <option key={index}>{col}</option>;
    });

    return <div className="cities">
        <h2>
            I'm a {value} product!
        </h2>
        <select value={value} onChange={(event) => setValue(event.target.value)}>
            {options}
        </select>

    </div>;
};

export default Task06;