import React from 'react';
import '../App.css';

function PersonTable() {
    return (
        <table className="person-table">
            <tbody>
            <tr>
                <th>First Name</th>
                <td>Oksana</td>
            </tr>
            <tr>
                <th>Last Name</th>
                <td>Bukhanevych</td>
            </tr>
            <tr>
                <th>Occupation</th>
                <td>Pirate Captain</td>
            </tr>
            </tbody>
        </table>
    )
}

export default PersonTable;