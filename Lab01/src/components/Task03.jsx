import React from 'react';

function Person (props) {
    return (
        <div>
            I'm a {props.names.surname} {props.names.name}.
        </div>
    )
}

export default Person;