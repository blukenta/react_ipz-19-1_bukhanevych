import React from 'react';
import ProductList from "./Task04_productList";
import img01 from "../pictures/01.jpg";
import img02 from "../pictures/02.jpg";
import img03 from "../pictures/03.jpg";
import img04 from "../pictures/04.jpg";
import img05 from "../pictures/05.jpg";
import img06 from "../pictures/06.jpg";
import img07 from "../pictures/07.jpg";
import img08 from "../pictures/08.jpg";

const Rozetka = () => {

    const products = [
        {
            id: 1, image: img01,
            title: "Стилус Adonit Mini 4 Orange (ADM4O)",
            price: 1009, oldPrice: 1999, status: 1
        },
        {
            id: 2, image: img02,
            title: "Стилус Ankndo ємнісний металевий двосторонній",
            price: 3199, status: 0
        },{
            id: 3, image: img03,
            title: "Стилус AIRON AirPen для ємнісного дисплея Black",
            price: 799, status: 1
        },{
            id: 4, image: img04,
            title: "Стилус AIRON AirPen для ємнісного дисплея White",
            price: 689
        }
    ]

    const productsAd = [
        {
            id: 1, image: img05,
            title: "Стилус Parblo A610 V2 (A610V2)",
            price: 2455, status: 0
        },
        {
            id: 2, image: img06,
            title: "Стилус Gaomon для малювання S620 чорний",
            price: 3199, oldPrice: 4999
        },{
            id: 3, image: img07,
            title: "Стилус Huion H420 з рукавичкою",
            price: 730, oldPrice: 899
        },{
            id: 4, image: img08,
            title: "Графічний планшет для малювання Huion Inspiroy",
            price: 1677, status: 1
        }
    ]

    return (
        <div className="rozetka-section">
            <ProductList products={products} title="Останні переглянуті товари"/>
            <ProductList products={productsAd} title="Реклама"/>
        </div>
    );
};

export default Rozetka;