import React from 'react';

const Status = ({status}) => {
    if (status === 1) {
        return (
            <div className="status">
                <div className="product-not-over">Закінчується</div>
            </div>
        );
    } else if (status === 0) {
        return (
            <div className="status">
                <div className="product-over">Немає в наявності</div>
            </div>
        );
    } else {
        return (
            <div className="no-status"></div>
        );
    }

};

export default Status;