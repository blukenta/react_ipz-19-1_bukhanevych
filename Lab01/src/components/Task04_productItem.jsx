import React from 'react';
import Price from "./Task04_price";
import Status from "./Task04_status";
import heart from "../pictures/heart.png"

const ProductItem = ({product}) => {
    return (
        <div className="product">
            <div className="block-img">
                <img className="img-fav" src={heart} alt="favorite"/>
                <img className="img-product" src={product.image} alt={product}/>
            </div>
            <div className="product-title">{product.title}</div>
            <div className="block-price">
                <Price oldPrice={product.oldPrice} price={product.price} />
            </div>
            <Status status={product.status} />
        </div>
    );
};

export default ProductItem;