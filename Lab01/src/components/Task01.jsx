import React from 'react';

const HelloWorld = function () {
    let myName = <strong>Oksana</strong>
    let myHeading = <h1>Hello, {myName}</h1>
    return (
        <div id="hello-world">{myHeading}</div>
    );
}

export default HelloWorld;
