import React from 'react';

const Cities = () => {

    const cities = [
        {name: "Chicago"},
        {name: "Paris"},
        {name: "Zhytomyr"}
    ]
    return (
        <select className="cities">
            {cities.map((city, index) =>
                <option key={index}>{city.name}</option>
            )}
        </select>
    );
};

export default Cities;