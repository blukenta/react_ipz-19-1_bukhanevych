import React from 'react';
import ProductItem from "./Task04_productItem";

const ProductList = ({products, title}) => {
    return (
        <div>
            <h3 className="block-title">
                {title}
            </h3>
            <div className="product-section">
                {products.map((product) =>
                    <ProductItem product={product} key={product.id}/>
                )}
            </div>
        </div>
    );
};

export default ProductList;