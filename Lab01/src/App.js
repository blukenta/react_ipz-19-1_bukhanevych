import React from 'react';
import './App.css';
import './components/Task01'
import HelloWorld from "./components/Task01";
import PersonTable from "./components/Task02";
import Person from "./components/Task03";
import Rozetka from "./components/Task04_rozetka";
import Cities from "./components/Task05";
import Task06 from "./components/Task06";

function App() {
    let props = {
        name: "Tomato",
        surname: "Red"
    }

    return (
        <div className="App">
            <HelloWorld />
            <PersonTable />
            <Person names={props} />
            <Rozetka />
            <Cities />
            <Task06 />
        </div>

    );
}

export default App;
