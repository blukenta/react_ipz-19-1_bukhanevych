import React from 'react';
import './App.css';
import MyCounter from "./components/MyCounter";
import Cart from "./components/Cart";
import GuessNum from "./components/GuessNum";

function App() {
    const counters = [
        {id: 1, initial: 6, min: -5, max: 10},
        {id: 2, initial: 5},
        {id: 3}
    ];

    return (
      <div className="App">
          {counters.map((counter) => (
            <MyCounter counter={counter} key={counter.id}/>
          ))}
          <Cart />
          <GuessNum />
      </div>
    );
}

export default App;
