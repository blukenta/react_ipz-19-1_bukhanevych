import React, {useState} from 'react';
import {Box, Typography} from "@mui/material";
import GameForm from "./GameProfile";

export default function GuessNum() {
    let [isRunning, setIsRunning] = useState(false);
    let [attemptsLeft, setAttemptsLeft] = useState(10);
    let [guesses, setGuesses] = useState([]);
    let [randomNumber, setRandomNumber] = useState(0);
    let [userNumber, setUserNumber] = useState(0);

    const startGame = () => {
        setIsRunning(true);
        setGuesses([]);
        setAttemptsLeft(10);
        setRandomNumber(Math.floor(Math.random() * 1000) + 1);
    }
    const decrementAttempts = () => {
        // console.log("decrementAttempts", attemptsLeft);
        if (attemptsLeft - 1 == 0) {
            alert(`Ви програли. Невгадане число: ${randomNumber}`);
            gameOver();
            return;
        }
        setAttemptsLeft((val) => val - 1);
    }
    const gameOver = () => {
        setIsRunning(false);
        setGuesses([]);
    }
    const guess = (number) => {
        if (number == randomNumber) {
            alert("Ви виграли!");
            gameOver();
            return;
        }

        setGuesses([...guesses, `N ${number < randomNumber ? ">" : "<"} ${number}`]);
        decrementAttempts();
    }

    const handleUserNumberChange = (event) => {
        setUserNumber(event.target.value);
    }

    return (
      <Box>

          <GameForm guess={guess} userNumber={userNumber} handleUserNumberChange={handleUserNumberChange}
                    startGame={startGame} isRunning={isRunning} />
          <Typography textAlign="center">
              {
                  isRunning ?
                    `Кількість спроб: ${attemptsLeft}` :
                    `Натисніть "Нова гра" щоб почати гру`
              }
          </Typography>
          <Box className="attempts">
              {
                  guesses.map((guess, index) => {
                        return <Typography key={index}>{guess}</Typography>
                    }
                  )}
          </Box>
      </Box>
    );
}
