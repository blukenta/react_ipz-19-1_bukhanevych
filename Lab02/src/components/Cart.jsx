import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import ProductsOfCart from "./ProductsOfCart";
import {useState} from "react";

function priceRow(price, quantity) {
    return price * quantity;
}

function createRow(id, name, price, quantity, min) {
    const total = priceRow(price, quantity);
    return { id, name, price, quantity, min, total };
}

function totals(items) {
    return items.map(({ total }) => total).reduce((sum, i) => sum + i, 0);
}

function totalQuantity(items) {
    return items.map(({ quantity }) => quantity).reduce((sum, i) => sum + i, 0);
}

const products = [
    createRow(1, 'Constructor LEGO', 300,1, 0),
    createRow(2, 'Train Station', 200,1, 0),
    createRow(3, 'Hot Wheels Track', 150,2, 0)
];

const invoiceTotals = totals(products);
const invoiceTotalQuantity = totalQuantity(products);


export default function SpanningTable() {
    const [itemsQuantity, setItemsQuantity] = useState(invoiceTotalQuantity);
    const [totalCost, setTotalCost] = useState(invoiceTotals);

    const updTotalQuantity = (quantity) => {
        setItemsQuantity(itemsQuantity + quantity);
    }

    const updTotalCost = (price) => {
        setTotalCost(totalCost + price);
    }

    return (
      <TableContainer component={Paper} sx={{ maxWidth: 700, my: 0, mx: 'auto' }}>
          <Table sx={{ maxWidth: 700 }} aria-label="spanning table">
              <TableHead>
                  <TableRow>
                      <TableCell align="left" colSpan={4} sx={{ fontSize: 24 }}>
                          Cart
                      </TableCell>
                  </TableRow>
                  <TableRow sx={{ background: '#c3e6cb' }} >
                      <TableCell>Name</TableCell>
                      <TableCell align="right">Price</TableCell>
                      <TableCell align="right">Quantity</TableCell>
                      <TableCell align="right">Total</TableCell>
                  </TableRow>
              </TableHead>
              <TableBody>
                  {products.map((product) => (
                    <ProductsOfCart product={product} key={product.id}
                                    updTotalQuantity={updTotalQuantity}
                                    updTotalCost={updTotalCost}/>
                  ))}

                  <TableRow sx={{ background: '#b8daff' }} >
                      <TableCell colSpan={2} align="left">Totals</TableCell>
                      <TableCell align="right">{itemsQuantity}</TableCell>
                      <TableCell align="right">{totalCost}</TableCell>
                  </TableRow>
              </TableBody>
          </Table>
      </TableContainer>
    );
}