import React, {useEffect, useState} from 'react';
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import {Button} from "@mui/material";

const ProductsOfCart = ({product, updTotalQuantity, updTotalCost}) => {
    const [count, setCount] = useState(initialState)

    const [qtyTotals, setQtyTotals] = useState()

    function incr() {
        setCount(count + 1)
        updTotalQuantity(1);
        updTotalCost(product.price);
    }

    function dec() {
        if (!product.min) {
            if (count > product.min) {
                setCount(count - 1)
                updTotalQuantity(-1);
                updTotalCost(-product.price);
            }
        } else {
            setCount(count)
        }
    }

    function initialState() {
        if (!product.quantity) {
            return 1;
        } else {
            return product.quantity;
        }
    }

    function stateQtyTotals(items) {
        return product.total = items * product.price
    }

    useEffect(() => {
        setQtyTotals(stateQtyTotals(count));
    },[count])

    return (
      <TableRow key={product.id}>
          <TableCell>{product.name}</TableCell>
          <TableCell align="right">{product.price}</TableCell>
          <TableCell align="right">
              <Button sx={{p: 1, fontSize: 14, mr: 2}} variant="outlined" onClick={incr}>
                  <i className="fa-solid fa-plus"></i>
              </Button>

              {count}

              <Button sx={{p: 1, fontSize: 14, ml: 2}} variant="outlined" onClick={dec}>
                  <i className="fa-solid fa-minus"></i>
              </Button>

          </TableCell>
          <TableCell align="right">
              {qtyTotals}
          </TableCell>
      </TableRow>
    );
};

export default ProductsOfCart;