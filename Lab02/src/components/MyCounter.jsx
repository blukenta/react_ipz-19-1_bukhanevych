import React, {useState} from 'react';
import {Button, Container, List, ListItem, ListItemText, Paper} from "@mui/material";

const MyCounter = ({counter}) => {

    const [count, setCount] = useState(initialState)

    function incr() {
        if (counter.max) {
            if (count < counter.max) {
                setCount(count + 1)
            }
        } else {
            setCount(count + 1)
        }
    }

    function dec() {
        if (counter.min) {
            if (count > counter.min) {
                setCount(count - 1)
            }
        } else {
            setCount(count - 1)
        }
    }

    function reset() {
        if (!counter.initial) {
            return setCount(count => 0);
        } else {
            return setCount(count => counter.initial);
        }
    }

    function initialState() {
        if (!counter.initial) {
            return 0;
        } else {
            return counter.initial;
        }
    }

    return (
      <div>
          <Container component="main" maxWidth="sm" sx={{md: 4}}>
              <Paper variant="outlined" sx={{my: {xs: 3, md: 3}, p: {xs: 2, md: 3}}}>
                  <List disablePadding>
                      <ListItem sx={{py: 1, px: 0}}>
                          <ListItemText>Поточний рахунок: {count}</ListItemText>
                          <Button sx={{mr: 1, p: 2, fontSize: 16}} variant="contained" onClick={incr}>
                              <i className="fa-solid fa-plus"></i>
                          </Button>

                          <Button sx={{mr: 1, p: 2, fontSize: 16}} variant="contained" onClick={dec}>
                              <i className="fa-solid fa-minus"></i>
                          </Button>
                          <Button variant="outlined" onClick={reset}>Reset</Button>
                      </ListItem>
                  </List>
              </Paper>
          </Container>
      </div>
    );
};

export default MyCounter;