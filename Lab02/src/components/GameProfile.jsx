import React from 'react';
import {Button, TextField} from "@mui/material"

export default function GameForm(props) {
    let { guess, userNumber, handleUserNumberChange, startGame, isRunning } = props;

    return (
        <form className="gameForm"
              onSubmit={(event) => {
                  event.preventDefault();
                  guess(userNumber);}}>
            <Button onClick={startGame} disabled={isRunning}>Нова гра</Button>
            <TextField variant="outlined" type="number" name="number_guess"
                       placeholder="Введіть число" disabled={!isRunning}
                       onChange={handleUserNumberChange} />
            <Button type="submit" disabled={!isRunning}>Перевірити</Button>
        </form>
    )
}